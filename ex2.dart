// Nattawat Sriinakidja 6550110004
void main() {
  print("\n");
  print("*** #4 List  ***");
  var values = [1, 3, 5, 7, 9];
  var sum = 0;

  for (var i = 0; i < values.length; i++) {
    sum += values[i];
  }

  print('Sum: \$$sum');



  print("\n");
  print("*** #5 Map ***");
  const pizzaPrices = {
    'margherita': 5.5,
    'pepperoni': 7.5,
    'vegetarian': 6.5,
  };

  const order = ['margherita', 'pepperoni'];

  double totalPrice = 0;

  for (var pizza in order) {
    if (pizzaPrices.containsKey(pizza)) {
      totalPrice += pizzaPrices[pizza]!;
    } else {
      print('$pizza pizza is not on the menu');
    }
  }

  print('Total Price: \$$totalPrice');

  print("\n");
  print("*** #6 function ***");

  print('total: \$${calculateTotal(pizzaPrices,order )}');
}


  double calculateTotal(Map<String, double> pizzaPrices, List<String> order) {

    var total = 0.0;
    for (var item in order) {
      final price = pizzaPrices[item];
      if (price != null) {
        total += price;
      } else {
        print('$item pizza is not on the menu');
      }
    }
    return total;
  }
