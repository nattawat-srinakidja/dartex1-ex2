// Nattawat Sriinakidja 6550110004
import 'dart:io';
void main() {

  print("\n");
  print("*** #1 Variables ***");
  double temperature = 20;
  int value = 2;
  String pizza = "pizza";
  String pasta = "pasta";

  String sentence = 'The temperature is ${temperature}C\n'
      '$value plus $value makes ${value + value}\n'
      'I like $pizza and $pasta';

  print(sentence);

  print("\n");
  print("*** #2 Temperature Conversion ***");
  double tempFahrenheit = 86;
  double tempCelsius = (tempFahrenheit - 32) / 1.8;
  print('${tempFahrenheit}F = ${tempCelsius.toStringAsFixed(1)}C');

print("\n");
  print("*** #3 IF-ELSE Condition - Salary ***");
  stdout.write("Enter your net salary: ");
  double netSalary = double.parse(stdin.readLineSync()!);

  stdout.write("Enter your expenses: ");
  double expenses = double.parse(stdin.readLineSync()!);

  if (netSalary > expenses) {
    double savings = netSalary - expenses;
    print("You have saved \$$savings this month.");
  } else if (expenses > netSalary) {
    double loss = expenses - netSalary;
    print("You have lost \$$loss this month.");
  } else {
    print("Your balance hasn't changed.");
  }

}
